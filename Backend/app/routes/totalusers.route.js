module.exports = app => {
    const customers = require("../controllers/totalusers.controller.js");
    app.get("/totalusers", customers.findAll);
};