module.exports = app => {
    const registration= require("../controllers/registration.controller.js");
    app.post("/registration",registration.userRegistration);
};