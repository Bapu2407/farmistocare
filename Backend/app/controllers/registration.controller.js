const registration = require("../models/registration.model.js");

exports.userRegistration = (req, res) => {
    // Validate request
    if (!req.body) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
    }
    registration.getByUserName(req.body.email, (err, data) => {
      if (data != true)  {
       registration.create(req.body.username,req.body.phoneno,req.body.email,req.body.password,(err, data) => {
          if (err !== "Data Inserted") {
            if (err.kind === "not_found") {
              res.status(404).send({
                message: "Not found UserName with id."
              });
            } else {
              res.status(200).send({
                message: "Somethings went wrong!"
              });
            }
          } else
          {
              res.status(200).send({
                message: true
              });
          } 
        });
      } else 
      {
        res.status(200).send({
          message: `Email Id Already exist.`
        });
      }
    });
  };
 
