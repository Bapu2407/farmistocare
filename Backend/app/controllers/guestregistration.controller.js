const registration = require("../models/guestregistration.model.js");

exports.guestRegistration = (req, res) => {
    // Validate request
    
    if (!req.body) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
    }
    registration.create(req.body.username,req.body.phoneno,req.body.email,req.body.gender,req.body.purpose,req.body.address,req.body.whometomeet,(err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found Customer with id ${req.params.customerId}.`
          });
        } else {
          res.status(500).send({
            message: "Could not delete Customer with id " + req.params.customerId
          });
        }
      } else
      {
          res.send(data.success);
      } 
    });
  };
 
