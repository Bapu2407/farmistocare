const registration = require("../models/sendmessage.model.js");

exports.Sendmessage = (req, res) => {
    // Validate request
    
    if (!req.body) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
    }
    registration.create(req.body.message,req.body.meetingid,(err, data) => {
      if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving customers."
      });
    else res.send(data);
    });
  };
 
