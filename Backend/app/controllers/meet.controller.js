const registration = require("../models/meet.model.js");

exports.userRegistration = (req, res) => {
    // Validate request
    
    if (!req.body) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
    }
    registration.create(req.body.meet,(err, data) => {
      if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving customers."
      });
    else res.send(data);
    });
  };
 
