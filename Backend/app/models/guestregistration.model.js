const sql = require("./db.js");

// constructor
const Register = function(reg) {
  this.username = reg.username;
  this.phoneno= reg.phoneno;
  this.email= reg.email;
  this.gender= reg.gender;
  this.purpose=reg.purpose;
  this.whometomeet=reg.whometomeet;
  this.address=reg.address;
 
};

Register.create = (username,phoneno,email,gender,purpose,address,whometomeet,result) => {
  sql.query('INSERT INTO guests (UserName,PhoneNo,Email,Gender,Purpose,Address,WhomeToMeet) VALUES (?,?,?,?,?,?,?)',[username,phoneno,email,gender,purpose,address,whometomeet], function (error, res, fields) {
    if (error) {
      console.log("error ocurred ", error);
      result(null, error);
    }else{
        result("Data Inserted",res);
       console.log('User Inserted');
       return;
    }
    });
};
module.exports = Register;