const sql = require("./db.js");

// constructor
const Register = function(reg) {
  this.username = reg.username;
  this.phoneno= reg.phoneno;
  this.email= reg.email;
  this.password= reg.password;
};

Register.create = (username,phoneno,email,password,result) => {
  sql.query('INSERT INTO users (UserName,PhoneNo,Email,Password) VALUES (?,?,?,?)',[username,phoneno,email,password], function (error, res, fields) {
    if (error) {
      console.log("error ocurred ", error);
      result(null, error);
    }else{
        result("Data Inserted",res);
       console.log('User Inserted');
       return;
    }
    });
};
Register.getByUserName = (email, result) => {
  sql.query('SELECT * FROM Users WHERE Email=?',[email], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found customer: ", res[0]);
      result(null, true);
      return;
    }

    // not found Customer with the id
    result({ kind: "not_found" }, null);
  });
};
module.exports = Register;