const express = require("express");
const bodyParser = require("body-parser");
var cors = require('cors');
const app = express();

app.use(cors());
// parse requests of content-type: application/json
app.use(bodyParser.json());

// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Access Denied..!" });
});       

// set port, listen for requests
// listen for requests
// Require Notes routes
require("./app/routes/customer.routes.js")(app);
require("./app/routes/login.route.js")(app);
require("./app/routes/registration.route.js")(app);
require("./app/routes/totalusers.route.js")(app);
require("./app/routes/guestregistration.route.js")(app);
require("./app/routes/allmeetings.route.js")(app);
require("./app/routes/getrole.route.js")(app);
require("./app/routes/meet.route.js")(app);
require("./app/routes/sendmessage.route.js")(app);

app.listen(3000, () => {
  console.log("Server is running on port 3000.");
});